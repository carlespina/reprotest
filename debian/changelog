reprotest (0.7.27) UNRELEASED; urgency=medium

  * Start 0.7.27 development. d/changelog entries will be written on
    release using the git commit messages.

 -- Holger Levsen <holger@debian.org>  Thu, 03 Aug 2023 16:40:20 +0200

reprotest (0.7.26) unstable; urgency=medium

  [ Vagrant Cascadian ]
  * tox.ini: Update to use "pytest" which is needed for tox 4. Thanks to
    Stefano Rivera. Closes: #1042918.

 -- Holger Levsen <holger@debian.org>  Thu, 03 Aug 2023 16:24:39 +0200

reprotest (0.7.25) unstable; urgency=medium

  * Add python3-wheel to build-depends. Thanks to Stefano Rivera.
    Closes: #1035645.

 -- Holger Levsen <holger@debian.org>  Sat, 13 May 2023 15:40:17 +0200

reprotest (0.7.24) unstable; urgency=medium

  [ Vagrant Cascadian ]
  * Fix build with both tox 4 and tox 3 using multi-line entry for tox.ini.
    Thanks to Stefano Rivera. Closes: #1035645.

  [ Holger Levsen ]
  * Be more verbose in d/changelog entry of previous release.
  * Release as 0.7.24.

 -- Holger Levsen <holger@debian.org>  Fri, 12 May 2023 14:49:28 +0200

reprotest (0.7.23) unstable; urgency=medium

  [ Vagrant Cascadian ]
  * Do not randomize chosen locale and always vary with the same UTF-8
    supported locale, et_EE.UTF-8, as this one has a different sort
    order than eg en_US.UTF-8. (Closes: #925879, #1004950)
  * Support passing --vary=locales.locale=LOCALE to specify locale to vary.
  * Document the above in README.

  [ Holger Levsen ]
  * README:
    - improve examples how to build a Debian package.
    - mention interesting locales in README.
  * Drop d/NEWS as it only contained one entry from 2017.

  [ lintian-brush ]
  * Bump standards version to 4.6.2, no changes needed.
  * Fill in Homepage field.

 -- Holger Levsen <holger@debian.org>  Mon, 20 Feb 2023 12:55:54 +0100

reprotest (0.7.22) unstable; urgency=medium

  [ Philip Hands ]
  * presets.py: include all files matching *.*deb in default artifact_pattern.
  * build.py: check setarch can run before including an arch, thus fixing
    salsa ci pipeline.
  * d/rules: emit an error message on version mismatch

  [ Holger Levsen ]
  * Release as 0.7.22.

 -- Holger Levsen <holger@debian.org>  Mon, 05 Sep 2022 14:40:41 +0200

reprotest (0.7.21) unstable; urgency=medium

  [ Nick Rosbrook ]
  * Dynamically tox environment in debian/rules instead of doing so
    statically in tox.ini.  LP: #1981624

  [ Mattia Rizzolo ]
  * Run the tests through pybuild to run them against all supported
    python3 versions.  Closes: #1016301
  * setup.cfg: fix a deprecation warning.

  [ Holger Levsen ]
  * build-depends: make diffoscope dependency unversioned as it's met even in
    Buster.
  * Bump standards version to 4.6.1.
  * Release as 0.7.21.

 -- Holger Levsen <holger@debian.org>  Fri, 29 Jul 2022 21:26:48 +0200

reprotest (0.7.20) unstable; urgency=medium

  [ Santiago Ruano Rincón ]
  * Add --append-build-command option. Closes: #1006923

  [ Holger Levsen ]
  * Release as 0.7.20.

 -- Holger Levsen <holger@debian.org>  Fri, 18 Mar 2022 14:40:41 +0100

reprotest (0.7.19) unstable; urgency=medium

  [ Stefano Rivera ]
  * Add diffoscope depends to autopkgtest. Closes: #999689
  * Allow random suffixes in the debian version compared to the upstream
    version. Closes: #999691

  [ Mattia Rizzolo ]
  * d/rules: make use of dh13 features to simplify some build targets.
  * Declaratively use dh_python3 through d/control.

  [ Holger Levsen ]
  * Release as 0.7.19.

 -- Holger Levsen <holger@debian.org>  Mon, 15 Nov 2021 15:14:30 +0100

reprotest (0.7.18) unstable; urgency=medium

  [ Vasyl Gello ]
  * Log selected variations on verbosity >= 1.
  * Implement realistic CPU architecture shuffling.

  [ Holger Levsen ]
  * setup.py: clarify that python3.9 is used.
  * Release as 0.7.18.

 -- Holger Levsen <holger@debian.org>  Wed, 13 Oct 2021 10:45:48 +0200

reprotest (0.7.17) unstable; urgency=medium

  [ Frédéric Pierret (fepitre) ]
  * Remove spec file as a specific branch will be created with it.
  * rpm: keep rpmbuild layout with RPMS folder.
  * test_reprotest: add spaces and regorganize functions.
  * test_reprotest: filter per distribution and register custom marks.
  * test_shell: Fix warnings about invalid escape sequences.

  [ Vagrant Cascadian ]
  * Make diffoscope optional. (Closes: #988964)

  [ Mattia Rizzolo ]
  * Set the diffoscope "optional dependency" the Python way.
  * Update doc to mention how `ldconfig` conflicts with the kernel variation.
    (Closes: #992694)

  [ Holger Levsen ]
  * Bump standards version to 4.6.0.
  * Release as 0.7.17.

 -- Holger Levsen <holger@debian.org>  Wed, 29 Sep 2021 22:22:09 +0200

reprotest (0.7.16) unstable; urgency=medium

  [ Holger Levsen ]
  * Update standards version to 4.5.1, no changes needed.
  * setup.py: bump version number for release.
  * tox.ini:
    - disable skip_missing_interpreters to catch failures early.
    - use python 3.9.

  [ Frédéric Pierret (fepitre) ]
  * Help on available values for verbosity.
  * logger: replace deprecated warn method.
  * Fix few deprecation warnings.
  * Update .gitignore.
  * presets:
    - slightly reorganize functions.
    - disable %clean stage when building RPM.
    - better formatting for RPM extra_build_command.
    - clean rpmbuild folder.
  * Update README for RPM support.
  * Add RPM spec file.
  * Add support for building source RPM.
  * tox.ini: passenv TERM.
  * test_reprotest.py: add note on expected error for -d.

  [ Marek Marczykowski-Górecki ]
  * Do not run tests on py38.
  * ci: don't run reprotest nested in reprotest.

 -- Holger Levsen <holger@debian.org>  Wed, 20 Jan 2021 16:44:02 +0100

reprotest (0.7.15) unstable; urgency=medium

  [ Holger Levsen ]
  * setup.py: bump version number for release.
  * Bump debhelper-compat to 13.

  [ Vagrant Cascadian ]
  * Update default arguments passed to diffoscope to support diffoscope
    version 153+. (Closes: #966256). Big thanks to Gregor Herrmann!
  * debian/control: Add myself to Uploaders.

 -- Vagrant Cascadian <vagrant@debian.org>  Sun, 26 Jul 2020 13:58:22 -0700

reprotest (0.7.14) unstable; urgency=medium

  [ Vagrant Cascadian ]
  * Add support for GNU Guix.

  [ Holger Levsen ]
  * setup.py: bump version number for release.

 -- Holger Levsen <holger@debian.org>  Mon, 10 Feb 2020 11:22:03 +0100

reprotest (0.7.13) unstable; urgency=medium

  [ Holger Levsen ]
  * setup.py:
    - update classifier to express we are using python3.7 now.
    - bump version number for this release.
  * Bump standards version to 4.5.0, no changes needed.

  [ Iñaki Malerba ]
  * Version test: split on + character.

 -- Holger Levsen <holger@debian.org>  Thu, 06 Feb 2020 01:57:30 +0100

reprotest (0.7.12) unstable; urgency=medium

  * README.rst: use "real" reStructuredText comments instead of using the "raw"
    directive.
  * setup.py:
    - update classifier to express we are using python3.7 now.
    - bump version number for this release.

 -- Holger Levsen <holger@debian.org>  Tue, 14 Jan 2020 16:35:04 +0100

reprotest (0.7.11) unstable; urgency=medium

  [ Ross Vandegrift ]
  * Allow user to override timeouts from the environment.

  [ Holger Levsen ]
  * Drop 'short' timeout and use 'install' timeout instead. Closes: #897442.
  * setup.py: bump version number for this release.

 -- Holger Levsen <holger@debian.org>  Tue, 14 Jan 2020 14:48:01 +0100

reprotest (0.7.10) unstable; urgency=medium

  [ Iñaki Malerba ]
  * Add --control-build parameter.
  * Update Salsa CI.

  [ Holger Levsen ]
  * setup.py: bump version number for the next release.
  * Bump standards version to 4.4.1, no changes needed.
  * Fix typo in previous changelog entry, thanks lintian.

 -- Holger Levsen <holger@debian.org>  Sat, 26 Oct 2019 13:26:10 +0200

reprotest (0.7.9) unstable; urgency=medium

  [ Dmitry Shachnev ]
  * reprotest/build.py:
    - do not use faketime at all when asked to not vary time. Closes: #930592.

  [ Ryan Scott ]
  * reprotest/__init__.py: fix --extra-build.

  [ Thomas Coldrick ]
  * reprotest/lib/adt_testbed.py: add FedoraInterface.

  [ AJ Jordan ]
  * reprotest/__init__.py: reference --no-clean-on-error in --store-dir text.

  [ Arnout Engelen ]
  * README.rst: add command-line example for make-based program.

  [ Chris Lamb ]
  * Add a .gitlab-ci.yml.
  * Publish built packages via aptly.

  [ Holger Levsen ]
  * setup.py: bump version number for the next release.
  * d/control:
    - bump standards version to 4.4.0, no changes needed.
    - drop debian/compat and build-depend on debhelper-compat instead.
    - bump build-depends on debhelper-compat=12, no changes
    - drop versioned recommends, the versions are fulfilled in oldstable.
    - stop declaring "X-Python3-Version: >= 3.5", this is the minimal version
      in oldstable anyway.
    - drop Ximin Luo from uploaders, thanks for all your work, Ximin!
    - add myself to uploaders.

 -- Holger Levsen <holger@debian.org>  Sun, 15 Sep 2019 18:08:10 +0200

reprotest (0.7.8) unstable; urgency=medium

  * Team upload.

  [ Holger Levsen ]
  * d/control: Declare that reprotest doesn't need root to build.

  [ Chris Lamb ]
  * Drop executable bit on doc/Makefile.
  * Add a bin/reprotest to match diffoscope.
  * Use our bin/reprotest wrapper in manpage generation to ensure we are
    using the local version of reprotest.
  * Update Vcs-{Git,Browser} as repository has been migrated to Salsa.
  * Update "Source" location in debian/copyright to point to new URI.
  * Update upstream "url" kwarg in setup.py.

  [ Mattia Rizzolo ]
  * Update README with updated instructions on how to release the tarball.
  * Update release documentation to also remember to push to PyPI.
  * d/control:
    + Bump Standards-Version to 4.1.5, no changes needed.
    + Drop Build-Depends on old version of dpkg-dev that is garanteed to exist.
    + Recommend diffoscope by itself instead as an alternative to diffutils.
    + Do not Recommend diffutils, that is an Essential:yes package.
    + Drop trailing full stop from the package synopsis.
  * Bump debhelper compat level to 11.
  * d/tests/control: Add missing dependency on fakeroot on the second test.
  * d/rules: Use dpkg's pkg-info.mk instead of dpkg-parsechangelog.
  * Fix spelling errors in documentation strings.
  * d/watch: Update to look at our new release archive.

 -- Mattia Rizzolo <mattia@debian.org>  Mon, 09 Jul 2018 15:33:05 +0200

reprotest (0.7.7) unstable; urgency=medium

  * Update debian/copyright and use HTTPS in debian/watch.

 -- Ximin Luo <infinity0@debian.org>  Fri, 15 Dec 2017 18:28:41 +0100

reprotest (0.7.6) unstable; urgency=medium

  * Ensure num_cpus variation uses a different number of cpus in different
    builds, and print a warning if only 1 cpu is available. (Closes: #884386)
  * Don't test the num_cpus variation if only 1 cpu is available.
  * Update to latest Standards-Version; no changes required.

 -- Ximin Luo <infinity0@debian.org>  Fri, 15 Dec 2017 16:39:08 +0100

reprotest (0.7.5) unstable; urgency=medium

  [ Santiago Torres ]
  * Use `uname -m` instead of `arch`.

  [ Ximin Luo ]
  * Allow the user to select a different path for diffoscope.
  * Allow --diffoscope-args to contain variables to be expanded.
  * Fix autopkgtest.

 -- Ximin Luo <infinity0@debian.org>  Thu, 14 Dec 2017 17:44:39 +0100

reprotest (0.7.4) unstable; urgency=medium

  * Hopefully fix the autopkgtest tests.
  * Add aslr, domain_host, and num_cpu variations.
  * Add a --print-sudoers feature.
  * Properly drop privs when running the build. (Closes: #877813)
  * Fix the time variation to actually make the time constant. This drops the
    time.auto_faketimes variation-spec option.
  * Make --no-clean-on-error a bit more reliable.

 -- Ximin Luo <infinity0@debian.org>  Mon, 27 Nov 2017 14:19:11 +0100

reprotest (0.7.3) unstable; urgency=medium

  * Fix --no-clean-on-error, it should work again.
  * Add a --env-build option to try to determine which (known and unknown)
    environment variables cause reproducibility.

 -- Ximin Luo <infinity0@debian.org>  Fri, 13 Oct 2017 16:48:04 +0200

reprotest (0.7.2) unstable; urgency=medium

  * Various bug fixes to get the basic dsc+schroot example working.
  * Improve the dsc+schroot preset to run builds as non-root.

 -- Ximin Luo <infinity0@debian.org>  Tue, 03 Oct 2017 19:25:11 +0200

reprotest (0.7.1) unstable; urgency=medium

  * New features:
    + Add a --auto-build option to try to determine which specific variations
      cause unreproducibility.
    + Add a --source-pattern option to restrict copying of source_root, and set
      this automatically in our presets.
  * Usability improvements:
    + Improve error messages in some common scenarios.
      - giving a source_root or build_command that doesn't exist
      - using reprotest with default settings after not installing Recommends
    + Output hashes after a successful --auto-build.
    + Print a warning message if we reproduced successfully but didn't vary
      everything.
  * Fix varying both umask and user_group at the same time.
  * Have dpkg-source extract to different build dir iff varying the build-path.
  * Pass --exclude-directory-metadata to diffoscope(1) by default as this is
    the majority use-case. Document the other cases in README and the man page.

 -- Ximin Luo <infinity0@debian.org>  Tue, 03 Oct 2017 15:56:25 +0200

reprotest (0.7) unstable; urgency=medium

  [ Ximin Luo ]
  * Document when one should use --diffoscope-args=--exclude-directory-metadata
    and do this in our Debian package presets.
  * Bump diffoscope Recommends version to >= 84 to support this flag.
  * Import autopkgtest 4.4, with minimal patches.
  * Choose an existent HOME for the control build. (Closes: #860428)
  * Add the ability to vary the user (Closes: #872412)
  * Heavy refactoring to support > 2 builds.
  * Add a variation config language to be able to configure the specifics of
    different variations, and to make it easier to configure further builds.
  * Deprecate the --dont-vary flag, add a --vary flag for better composability.
  * Support >2 builds using the new --extra-build flag.
  * Properly sanitize artifact_pattern to avoid arbitrary shell execution.
  * Update to Standards-Version 4.1.0.

  [ Mattia Rizzolo ]
  * Use https for the Format URI in debian/copyright.
  * Bump debhelper compat level to 10.

  [ Santiago Torres ]
  * Abstract parts of autopkgtest to support running on non-Debian systems.
  * Add a --host-distro flag to support that too.

 -- Ximin Luo <infinity0@debian.org>  Tue, 19 Sep 2017 14:18:18 +0200

reprotest (0.6.2) unstable; urgency=medium

  * Add a documentation section on "Known bugs".
  * Move developer documentation away from the man page.
  * Mention release instructions in the previous changelog.

 -- Ximin Luo <infinity0@debian.org>  Tue, 09 May 2017 21:56:22 +0200

reprotest (0.6.1) unstable; urgency=medium

  [ Ximin Luo ]
  * Preserve directory structure when copying artifacts. Otherwise hash output
    on a successful reproduction sometimes fails, because find(1) can't find
    the artifacts using the original artifact_pattern.

  [ Chris Lamb ]
  * Add proper release instructions and a keyring.

 -- Ximin Luo <infinity0@debian.org>  Tue, 09 May 2017 19:56:29 +0200

reprotest (0.6) unstable; urgency=medium

  * When a reproduction succeeds, only output hashes of the artifacts rather
    than everything in the current directory.
  * Remove unimplemented variations from README.
  * Rename some variations:
      path -> exec_path, to distinguish it from build_path
      faketime -> time, since users care about interface not implementation
  * Fix a bug involving --store-dir and hash output.
  * Add a --config-file option and fix the loading of configs.
  * Add a man page. (Closes: #833282)
  * Improve logging so it's properly controlled by --verbosity.

 -- Ximin Luo <infinity0@debian.org>  Tue, 24 Jan 2017 22:19:34 +0100

reprotest (0.5) unstable; urgency=medium

  * Stop advertising variations that we're not actually varying.
    That is: domain_host, shell, user_group.
  * Fix auto-presets in the case of a file in the current directory.
  * Allow disabling build-path variations. (Closes: #833284)
  * Add a faketime variation, with NO_FAKE_STAT=1 to avoid messing with
    various buildsystems. This is on by default; if it causes your builds
    to mess up please do file a bug report.
  * Add a --store-dir option to save artifacts.

 -- Ximin Luo <infinity0@debian.org>  Fri, 06 Jan 2017 20:18:45 +0100

reprotest (0.4) unstable; urgency=medium

  * Document virtual servers and caveats better.
  * Add a --help [virtual server] option.
  * Add a --no-diffoscope option. (Closes: #844512)
  * Add a --testbed-init option to allow the user to install dependencies that
    are needed to make the variations in the first place.
  * Add an "auto" feature to the CLI, plus a presets module so it's easier to
    use, and other non-Debian systems can start populating this too.
  * Remove autopkgtest manpages and other unused files. (Closes: #834300)

 -- Ximin Luo <infinity0@debian.org>  Mon, 28 Nov 2016 21:34:04 +0100

reprotest (0.3.2) unstable; urgency=medium

  * Add a --diffoscope-arg option to pass extra args to diffoscope.

 -- Ximin Luo <infinity0@debian.org>  Mon, 26 Sep 2016 18:49:41 +0200

reprotest (0.3.1) unstable; urgency=medium

  * Make some variations more reliable, so tests don't fail.

 -- Ximin Luo <infinity0@debian.org>  Fri, 23 Sep 2016 22:03:30 +0200

reprotest (0.3) unstable; urgency=medium

  [ Daniel Kahn Gillmor ]
  * Add README.md to the binary package. (Closes: #834299)

  [ Ximin Luo ]
  * Support pattern matching in the build artifact, which can match more than
    one build artifact; see README.md for details on using this.
  * Add a --no-clean-on-error flag so you can analyse anything that failed or
    did not reproduce.
  * Don't fail when the build process has stderr. (Closes: #836517)
  * Fix tests, and run them during the build. disorderfs is disabled for now,
    but only when running the tests via debian/rules.

 -- Ximin Luo <infinity0@debian.org>  Fri, 23 Sep 2016 20:39:09 +0200

reprotest (0.2) unstable; urgency=medium

  * Add support for schroot and qemu.

 -- Ceridwen <ceridwenv@gmail.com>  Sat, 11 Jun 2016 18:05:05 -0400

reprotest (0.1) unstable; urgency=medium

  * Initial release.

 -- Ceridwen <ceridwenv@gmail.com>  Sat, 11 Jun 2016 18:05:05 -0400
